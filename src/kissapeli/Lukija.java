    package kissapeli;

import java.util.Scanner;

public class Lukija {

        private Scanner scan;
        public String nimi;
        
        public Lukija(){
            scan = new Scanner(System.in);
        }
        public String readName(){
            while(true){
                nimi = scan.nextLine();
                if (nimi.length() < 21 && nimi.length() > 0) break;
                System.out.println("Name too long");
            }
            return "";
        }
}
