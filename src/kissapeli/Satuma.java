package kissapeli;
import java.util.Random;

public class Satuma {
    
    private String[] etu;
    private String[] keski;
    private String[] loppu;
    
    private Random randi;
    
    //NIMET
    
    public Satuma(){
        randi = new Random();
        etu = new String[]{"Greater", "Master", "Santeri", "Dank", "Dat"};
        keski = new String[]{"Head", "Black", "Moon", "Gnome", "Tall", ""};
        loppu = new String[]{"bassist", "warrior", "Moon", "child", "Boi", "skrub"};
    }
    
    // NUMERO 1-99
    
    public int getNumber(){
        return randi.nextInt(100);
    }
    
    //NIMI
   
    public String getName(){
        int i = randi.nextInt(etu.length);
        int j = randi.nextInt(keski.length);
        int k = randi.nextInt(loppu.length);
        return etu[i] + " " + keski [j] + loppu [k];
    }
    
}
