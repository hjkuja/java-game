package Entities;

import gfx.Colours;
import gfx.Screen;
import Level.Level;

public class Kissa extends Mob{
	
	private int colour = Colours.get(-1, 111, 45, 543);
    private int scale = 1;
    protected boolean isSwimming = false;
    private int tickCount = 0;

	public Kissa(Level level, String name, int x, int y, int speed) {
		super(level, name, x, y, speed);
	}

	@Override
	public boolean hasCollided(int xa, int ya) {
		return false;
	}

	@Override
	public void tick() {
		
	}

	@Override
	public void render(Screen screen) {
		int xTile = 0;
        int yTile = 28;
	}

}
