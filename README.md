# Java game

A Java game I made in 2015-2016.

**This was made following a tutorial.**

[Link to demo video on YouTube](https://youtu.be/RoMfEblDX_g)

[Download the demo](/jar/demo.jar)



## Features

* Moving with WASD
* Name
* Map is modifiable straight from a png image
     * [Link to level images](src/Res/)
* Multiplayer partly ready

## Screenshot from the game

![Screenshot](ss.png)